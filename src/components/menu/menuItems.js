let menuItems = {
    new: 'Новые заказы',
    my: 'Мои заказы',
    stat: 'Статистика',
    help: 'Помощь',
};

export default menuItems;