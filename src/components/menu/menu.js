import React, { Component } from 'react';
import { connect } from 'react-redux';
import store from './../../store/index';
import menuItems from './menuItems';

class Menu extends Component {
    constructor(props) {
        super(props);
        this.state = {
            newTaskCount: 0,
            myTaskCount: 0
        };
        this.click = this.click.bind(this);
        this.getNewCount = this.getNewCount.bind(this);
        this.getMyCount = this.getMyCount.bind(this);
    }
    click(e, item) {
        if (this.props.menu.item !== item) {
            store.dispatch({
                type: 'MENU_ACTIVE_ITEM',
                item: item
            });
            store.dispatch({
                type: 'MENU_OPEN',
                open: false
            });
            store.dispatch({
                type: 'BTN_STATE',
                btnState: 'menu'
            });
            let selectedOption = null;
            if (item === menuItems.new) selectedOption = 'all';
            else if (item === menuItems.my) selectedOption = 'active';
            store.dispatch({
                type: 'OPTIONS_SELECT',
                optionsSelected: selectedOption,
            });
        }
    }
    exit() {
        let xhr = new XMLHttpRequest();
        xhr.open('POST', '/performer/auth/logout', true);
        xhr.send();
        xhr.onreadystatechange = () => {
            if (xhr.readyState !== 4) return;
            if (xhr.status !== 200) console.log(xhr.status + ': ' + xhr.statusText);
            else window.location.reload(true);
        }
    }
    getNewCount() {
        let xhr = new XMLHttpRequest();
        xhr.open('POST', 'performer/task/list/count/new', true);
        xhr.send();
        xhr.onreadystatechange = () => {
            if (xhr.readyState !== 4) return;
            if (xhr.status !== 200) console.log(xhr.status + ': ' + xhr.statusText);
            else {
                let count = JSON.parse(xhr.responseText);
                this.setState({
                    newTaskCount: count.count,
                });
            }
        }
    }
    getMyCount() {
        let xhr = new XMLHttpRequest();
        xhr.open('POST', 'performer/task/list/count/my', true);
        xhr.send();
        xhr.onreadystatechange = () => {
            if (xhr.readyState !== 4) return;
            if (xhr.status !== 200) console.log(xhr.status + ': ' + xhr.statusText);
            else {
                let count = JSON.parse(xhr.responseText);
                this.setState({
                    myTaskCount: count.count,
                });
            }
        }
    }
    componentDidMount() {
        //this.getNewCount();
        //this.getMyCount();
    }
    componentWillReceiveProps(nextProps) {
        //this.getNewCount();
        //this.getMyCount();
      }
    render() {
        let styles = {
            menu: {
                position: 'absolute',
                zIndex: '1',
                width: '100%',
                minHeight: 'calc(100vh - 60px)',
                marginLeft: this.props.menu.open ? '0' : '-100%',
                backgroundColor: '#192227',
                transition: '.4s',
                display: 'flex',
                flexDirection: 'column',
                justifyContent: 'flex-start',
                alignItems: 'flex-start',
            },
            info: {
                paddingTop: '20px',
                paddingLeft: '10px',
                color: '#ffffff',
                fontSize: '12px',
            },
            itemsCont: {
                width: '100%',
                marginTop: '20px',
            },
            items: {
                width: 'calc(100% - 20px)',
                padding: '20px 10px',
                color: '#ffffff',
                display: 'flex',
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
            },
            selectedItem: {
                backgroundColor: 'rgba(0,0,0,0.3)',
                width: 'calc(100% - 20px)',
                padding: '20px 10px',
                color: '#ffffff',
                display: 'flex',
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
            },
            exitBtn: {
                width: 'calc(100% - 20px)',
                padding: '40px 10px',
                color: '#e63d32',
                display: 'flex',
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
            }
        };
        return (
            <div style={styles.menu}>
                <div style={styles.info}>
                    <div>
                        {this.props.user.name}
                    </div>
                    <div>
                        {this.props.user.mail}
                    </div>
                    <div>
                        {this.props.user.id}
                    </div>
                </div>
                <div style={styles.itemsCont}>
                    <div onClick={(e)=>{this.click(e, menuItems.new)}} style={this.props.menu.item === menuItems.new ? styles.selectedItem : styles.items}>
                        <div>
                            {menuItems.new}
                        </div>
                        <div>
                            {/*this.state.newTaskCount*/}
                        </div>
                    </div>
                    <div onClick={(e)=>{this.click(e, menuItems.my)}} style={this.props.menu.item === menuItems.my ? styles.selectedItem : styles.items}>
                        <div>
                            {menuItems.my}
                        </div>
                        <div>
                            {/*this.state.myTaskCount*/}
                        </div>
                    </div>
                    <div onClick={(e)=>{this.click(e, menuItems.stat)}} style={this.props.menu.item === menuItems.stat ? styles.selectedItem : styles.items}>
                        <div>
                            {menuItems.stat}
                        </div>
                    </div>
                    <div onClick={(e)=>{this.click(e, menuItems.help)}} style={this.props.menu.item === menuItems.help ? styles.selectedItem : styles.items}>
                        <div>
                            {menuItems.help}
                        </div>
                    </div>
                    <div onClick={this.exit} style={styles.exitBtn}>
                        Выход
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = function(store) {
    return {
        menu: store.menu,
        btn: store.btn,
        options: store.options,
    };
};
  
export default connect(mapStateToProps)(Menu);