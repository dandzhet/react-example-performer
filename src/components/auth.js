import React, {Component} from 'react';
import {connect} from 'react-redux';
import store from './../store/index';

class Auth extends Component {
    constructor(props) {
        super(props);
        this.state = {};
        this.send = this.send.bind(this);
    }

    send(e) {
        e.preventDefault();
        let dataFromInputs = {};
        for (let i = 0; i < e.target.length - 1; i++) dataFromInputs[e.target[i].name] = e.target[i].value;
        console.log(dataFromInputs);
        fetch('/performer/auth', {method: 'POST', headers: {'Accept': 'application/json', 'Content-Type': 'application/json'}, body: JSON.stringify(dataFromInputs)})
            .then(res => {
                if (res.status !== 200) return console.log('Err: ' + res.statusText + ' ' + res.status);
                res.json().then((data) => {
                    if (data.status === 'ok') {
                        store.dispatch({type: 'AUTH', auth: true});
                    }
                    else alert('Попробуйте еще раз');
                });
            });
    }

    render() {
        let styles = {
            general: {
                width: '100%',
                height: '100vh',
                backgroundColor: '#232c31',
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
                justifyContent: 'center',
            },
            form: {
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
                justifyContent: 'center',
            },
            mailInpt: {
                width: '200px',
                padding: '10px',
                border: 'none',
                outline: 'none',
            },
            passInpt: {
                width: '200px',
                padding: '10px',
                marginTop: '10px',
                border: 'none',
                outline: 'none',
            },
            sendBtn: {
                width: '220px',
                padding: '10px',
                marginTop: '10px',
                backgroundColor: '#4CAF50',
                border: 'none',
                outline: 'none',
                color: '#ffffff',
            },
        };
        return (
            <div style={styles.general}>
                <form style={styles.form} onSubmit={this.send}>
                    <input style={styles.mailInpt} placeholder='Почта' name='mail' required type='mail'/>
                    <input style={styles.passInpt} placeholder='Пароль' name='password' required type='password'/>
                    <button style={styles.sendBtn} type="submit">Отправить</button>
                </form>
            </div>
        );
    }
}

const mapStateToProps = store => ({menu: store.menu, btn: store.btn, options: store.options, detail: store.detail});
export default connect(mapStateToProps)(Auth);