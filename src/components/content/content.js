import React, { Component } from 'react';
import { connect } from 'react-redux';
//import store from './../../store/index';

import Auth from './../auth';
import General from './general/general';
import Detail from './detail/detail';

class Content extends Component {
  constructor(props){
    super(props);
    this.state = {};
  }
  componentDidMount() {
    console.log(this.props.auth.auth);
  }
  render() {
    let style = {
        width: '100%',
        display: 'flex',
        flexDirection: 'row',
    };
    return (
      <div className="Content" style={style}>
        {!this.props.auth.auth ? <Auth/> : null}
        {this.props.auth.auth ? <General/> : null}
        {this.props.auth.auth ? <Detail/> : null}
      </div>
    );
  }
}

const mapStateToProps = function(store) {
  return {
      auth: store.auth,
      menu: store.menu,
      btn: store.btn,
      options: store.options,
      params: store.params
  };
};

export default connect(mapStateToProps)(Content);