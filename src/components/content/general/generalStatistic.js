import React, { Component } from 'react';
import { connect } from 'react-redux';
//import store from './../../../store/index';

class GeneralStatistic extends Component {
  constructor(props){
    super(props);
    this.state = {};
  }
  componentDidMount() {

  }
  render() {
    let styles = {
      general: {
        padding: '5px',
        color: '#ffffff',
        fontSize: '12px',
      },
      item: {
        padding: '5px 0'
      },
    };
    return (
      <div style={styles.general}>
        <div style={styles.item}>
          Заказов выполнено: 0
        </div>
        <div style={styles.item}>
          Денег заработано: 0
        </div>
      </div>
    );
  }
}

const mapStateToProps = function(store) {
  return {
      menu: store.menu,
      btn: store.btn,
      options: store.options,
      detail: store.detail
  };
};

export default connect(mapStateToProps)(GeneralStatistic);