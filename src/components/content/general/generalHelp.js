import React, { Component } from 'react';
import { connect } from 'react-redux';
//import store from './../../../store/index';

class GeneralHelp extends Component {
  constructor(props){
    super(props);
    this.state = {};
  }
  componentDidMount() {

  }
  render() {
    let styles = {
      general: {
        color: '#ffffff',
        padding: '5px',
        display: 'flex',
        flexDirection: 'column',
        justifuContent: 'center',
        alignItems: 'center',
      },
      text: {
        marginTop: '40px',
        fontSize: '18px',
        fontWeight: '900',
      },
      phone: {
        marginTop: '40px',
        padding: '20px',
        fontSize: '16px',
        backgroundColor: '#4CAF50',
        textDecoration: 'none',
        color: '#ffffff',
      }
    };
    return (
      <div style={styles.general}>
        <div style={styles.text}>
          По всем вопросам звонить
        </div>
        <a style={styles.phone} href={'tel:+79131234567'}>
          +7 (913) 123-45-67
        </a>
      </div>
    );
  }
}

const mapStateToProps = function(store) {
  return {
      menu: store.menu,
      btn: store.btn,
      options: store.options,
      detail: store.detail
  };
};

export default connect(mapStateToProps)(GeneralHelp);