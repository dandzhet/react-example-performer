import React, {Component} from 'react';
import {connect} from 'react-redux';
import store from './../../../store/index';

class GeneralMy extends Component {
    constructor(props) {
        super(props);
        this.state = {
            task: [
                {
                    id: '- - -',
                    address: '- - -',
                    type: '- - -',
                    work: ['- - -'],
                    date: '- - -',
                    price: '- - -',
                }
            ]
        };
        GeneralMy.click = GeneralMy.click.bind(this);
        this.getData = this.getData.bind(this);
    }
    getData() {
        fetch('/performer/applications/my', {method: 'POST'})
            .then(res => res.json())
            .then(resJson => {
                console.log(resJson);
                resJson.data = resJson.data.map(el => {
                    if (el.type === 'look') {
                        el.work = el.work.map(el => {
                            if (el === 'roof') return 'Кровля';
                            else if (el === 'aprons') return 'Кровля';
                            else if (el === 'perimeter') return 'Периметр';
                            return el;
                        });
                        el.type = 'Осмотр';
                    } else if (el.type === 'exec') {
                        let work = [];
                        if (el.work.hasOwnProperty('roof')) work.push('Кровля');
                        if (el.work.hasOwnProperty('aprons')) work.push('Козырьки');
                        if (el.work.hasOwnProperty('perimeter')) work.push('Периметр');
                        el.work = work;
                        el.type = 'Выполнение';
                    }
                    el.price = el.price ? el.price : '- - -';
                    el.date = el.date ? GeneralMy.getStringFromDate(el.date) : '- - -';
                    return el;
                });
                this.setState({task: resJson.data});
            })
    }
    static getStringFromDate(date) {
        let dateObj = new Date(date);
        let minutes = dateObj.getMinutes().toString().length === 1 ? '0' + dateObj.getMinutes() : dateObj.getMinutes();
        return dateObj.toISOString().substring(0, 10) + ' ' + dateObj.getHours() + ':' + minutes;
    }
    static click(evt, itemId, performerDataId) {
        store.dispatch({type: 'DETAIL_INFO', detailInfo: itemId});
        store.dispatch({type: 'DETAIL_DATA', detailData: performerDataId});
        store.dispatch({type: 'DETAIL', detail: '0%'});
        store.dispatch({type: 'BTN_STATE', btnState: 'back'});
    }
    componentDidMount() {this.getData()}
    componentWillReceiveProps(nextProps) {this.getData()}
    render() {
        let generalStyle = {
            width: '100%',
            paddingBottom: '5px',
            display: 'flex',
            flexDirection: 'column',
            transition: '.4s'
        };
        let itemStyle = {
            width: 'calc(100% - 20px)',
            padding: '10px',
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'flex-start',
            justifyContent: 'flex-start',
        };
        let itemTitleStyle = {
            color: '#ffffff',
        };
        let itemTextStyle = {
            padding: '5px 0',
            color: '#757575',
            fontSize: '12px',
            display: 'flex',
            flexDirection: 'row',
        };
        let itemDatePrice = {
            width: '100%',
            display: 'flex',
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
        };
        let itemPriceStyle = {
            color: '#ffffff',
            fontSize: '12px',
        };
        let itemDate = {
            color: '#ffffff',
            fontSize: '12px',
        };
        let items = this.state.task.map(item => {
            return (
                <div key={item.id} style={itemStyle} onClick={e => GeneralMy.click(e, item.id)}>
                    <div style={itemTitleStyle}>{item.address}</div>
                    <div style={itemTextStyle}>
                        <div style={{marginRight: '5px'}}>{item.type}:</div>
                        {item.work.map((el, i) => <div key={i} style={{marginRight: '2px'}}>{el}{item.work.length - 1 > i ? ',' : null} </div>)}
                    </div>
                    <div style={itemDatePrice}>
                        <div style={itemDate}>{item.date}</div>
                        <div style={itemPriceStyle}>{(item.price + ' руб')}</div>
                    </div>
                </div>
            );
        });
        return <div className="General" style={generalStyle}>{items}</div>;
    }
}

const mapStateToProps = store => ({btn: store.btn, options: store.options, detail: store.detail});
export default connect(mapStateToProps)(GeneralMy);