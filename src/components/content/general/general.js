import React, { Component } from 'react';
import { connect } from 'react-redux';
//import store from './../../../store/index';
import menuItems from './../../menu/menuItems';
import GeneralNew from './generalNew';
import GeneralMy from './generalMy';
import GeneralStatistic from './generalStatistic';
import GeneralHelp from './generalHelp';

class General extends Component {
  constructor(props){
    super(props);
    this.state = {};

  }
  componentDidMount() {
    
  }
  render() {
    let styles = {
      general: {
        width: '100%',
        height: this.props.detail.detail === '0%' ? '0' : '',
      },
    };
    return (
      <div style={styles.general}>
        {this.props.menu.item === menuItems.new ? <GeneralNew/> : null}
        {this.props.menu.item === menuItems.my ? <GeneralMy/> : null}
        {this.props.menu.item === menuItems.stat ? <GeneralStatistic/> : null}
        {this.props.menu.item === menuItems.help ? <GeneralHelp/> : null}
      </div>
    );
  }
}

const mapStateToProps = function(store) {
  return {
      menu: store.menu,
      btn: store.btn,
      options: store.options,
      detail: store.detail
  };
};

export default connect(mapStateToProps)(General);