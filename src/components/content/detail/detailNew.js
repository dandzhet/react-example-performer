import React, {Component} from 'react';
import {connect} from 'react-redux';
import store from './../../../store/index';

class DetailNew extends Component {
    constructor(props) {
        super(props);
        this.state = {
            task: {
                date: '- - -',
                address: '- - -',
                type: '- - -',
                work: ['- - -'],
                other: '- - -',
                price: '- - -',
            }
        };
        this.getData = this.getData.bind(this);
        this.getTask = this.getTask.bind(this);
    }

    getData() {
        fetch('/performer/applications/new/' + this.props.detail.detailInfo, {method: 'POST'})
            .then(res => res.json())
            .then(resJson => {
                console.log(resJson);
                if (resJson.data.type === 'look') {
                    resJson.data.work = resJson.data.work.map(el => {
                        if (el === 'roof') return 'Кровля';
                        else if (el === 'aprons') return 'Кровля';
                        else if (el === 'perimeter') return 'Периметр';
                        return el;
                    });
                    resJson.data.type = 'Осмотр';
                } else if (resJson.data.type === 'exec') {
                    let work = [];
                    if (resJson.data.work.hasOwnProperty('roof')) work.push('Кровля');
                    if (resJson.data.work.hasOwnProperty('aprons')) work.push('Козырьки');
                    if (resJson.data.work.hasOwnProperty('perimeter')) work.push('Периметр');
                    resJson.data.work = work;
                    resJson.data.type = 'Выполнение';
                }
                resJson.data.date = resJson.data.date ? resJson.data.date : '- - -';
                this.setState({task: resJson.data});
            })
    }

    static getStringFromDate(date) {
        let dateObj = new Date(date);
        let minutes = dateObj.getMinutes().toString().length === 1 ? '0' + dateObj.getMinutes() : dateObj.getMinutes();
        return dateObj.toISOString().substring(0, 10) + ' ' + dateObj.getHours() + ':' + minutes;
    }

    getTask() {
        fetch('/performer/applications/get/' + this.props.detail.detailInfo, {method: 'POST'})
            .then(res => res.status !== 200 ? console.log('Err: ' + res.statusText + ' ' + res.status) : res.json())
            .then(resJson => {
                console.log(resJson);
                if (resJson.status === 'ok') {
                    store.dispatch({type: 'BTN_STATE', btnState: 'menu'});
                    store.dispatch({type: 'DETAIL', detail: '-100%'});
                }
            });
    }

    componentDidMount() {this.getData()}

    render() {
        let styles = {
            general: {
                width: '100%',
                color: '#ffffff',
            },
            info: {
                padding: '5px 0',
            },
            infoItem: {
                padding: '5px 0',
                display: 'flex',
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'flex-start',
            },
            infoItemName: {
                width: '100px',
                fontSize: '12px',
                fontWeight: '900',
            },
            infoItemText: {
                fontSize: '12px',
                fontWeight: '100',
                color: '#eeeeee'
            },
            btn: {
                width: '100%',
                margin: '5px 0',
                padding: '20px 0',
                backgroundColor: '#4CAF50',
                fontSize: '14px',
                textAlign: 'center',
            },
        };
        return (
            <div style={styles.general}>
                {this.props.detail.detailInfo ?
                    (
                        <div style={styles.info}>
                            <div style={styles.infoItem}>
                                <div style={styles.infoItemName}>Приступить:</div>
                                <div style={styles.infoItemText}>{this.state.task.date}</div>
                            </div>
                            <div style={styles.infoItem}>
                                <div style={styles.infoItemName}>Адрес работ:</div>
                                <div style={styles.infoItemText}>{this.state.task.address}</div>
                            </div>
                            <div style={styles.infoItem}>
                                <div style={styles.infoItemName}>Тип заказа:</div>
                                <div style={styles.infoItemText}>{this.state.task.type}</div>
                            </div>
                            <div style={styles.infoItem}>
                                <div style={styles.infoItemName}>Виды работ:</div>
                                <div style={{fontSize: '12px', fontWeight: '100', color: '#eeeeee', display: 'flex', flexDirection: 'row'}}>
                                    {this.state.task.work.map((el, i) => <div key={i} style={{marginRight: '5px'}}>{el}{this.state.task.work.length - 1 > i ? ',' : null}</div>)}
                                </div>
                            </div>
                            <div style={styles.infoItem}>
                                <div style={styles.infoItemName}>Доп. инфо.:</div>
                                <div style={styles.infoItemText}>{this.state.task.other ? this.state.task.other : '- - -'}</div>
                            </div>
                            <div style={styles.infoItem}>
                                <div style={styles.infoItemName}>З.п.:</div>
                                <div
                                    style={styles.infoItemText}>{this.state.task.price ? (this.state.task.price + ' руб.') : '- - -'}</div>
                            </div>
                            <div onClick={this.getTask} style={styles.btn}>
                                Взять заказ
                            </div>
                        </div>
                    )
                    :
                    '- - -'
                }
            </div>
        );
    }
}

const mapStateToProps = store => ({detail: store.detail, btn: store.btn});
export default connect(mapStateToProps)(DetailNew);