import React, {Component} from 'react';
import {connect} from 'react-redux';
//import store from './../../../store/index';
import menuItems from './../../menu/menuItems';
//import Info from './info/info';
//import Forms from './forms/forms';
import DetailNew from './detailNew';
import DetailMy from './detailMy';

class Detail extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }
    componentDidMount() {

    }
    render() {
        let generalStyle = {
            position: 'absolute',
            width: 'calc(100% - 20px)',
            minHeight: 'calc(100vh - 60px)',
            padding: '0 10px',
            right: this.props.detail.detail,
            top: '60px',
            backgroundColor: '#232c31',
            transition: '.4s',
        };
        return (
            <div style={generalStyle}>
                {this.props.detail.detail === '0%' ? (
                    <div>
                        {this.props.menu.item === menuItems.new ? <DetailNew/> : null}
                        {this.props.menu.item === menuItems.my ? <DetailMy/> : null}
                    </div>
                ) : null}
            </div>
        );
    }
}

const mapStateToProps = function (store) {
    return {
        menu: store.menu,
        btn: store.btn,
        options: store.options,
        detail: store.detail,
    };
};

export default connect(mapStateToProps)(Detail);