import React, { Component } from 'react';

class Contacts extends Component {
  render() {
    let contactsStyle = {
        width: 'calc(100% - 10px)',
        height: '48px',
        marginTop: '5px',
        marginBottom: '15px',
        borderBottom: '1px solid #4CAF50',
        borderTop: '1px solid #4CAF50',
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    };
    let contactsTextStyle = {
        color: '#ffffff'
    };
    let contactsTextNameStyle = {
        fontWeight: '800',
        fontSize: '14px',
    };
    let contactsTextPhoneStyle = {
        paddingTop: '2px',
        fontSize: '14px'
    };
    let contactsAStyle = {
        marginRight: '-10px',
        borderRadius: '10px',
        boxShadow: '0 0 5px -1px #000000',
        fontSize: '14px',
        padding: '18px 20px',
        backgroundColor: '#4CAF50',
        color: '#ffffff',
        textDecoration: 'none',
    };
    return (
    <div style={contactsStyle}>
        <div style={contactsTextStyle}>
            <div style={contactsTextNameStyle}>{this.props.name}</div>
            <div style={contactsTextPhoneStyle}>{this.props.phone}</div>
        </div>
        <a style={contactsAStyle} href={"tel:" + this.props.phone}>Позвонить</a>
    </div>
    );
  }
}

export default Contacts;