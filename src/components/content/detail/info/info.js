import React, { Component } from 'react';
import { connect } from 'react-redux';
//import store from './../../../../store/index';

import Contacts from './contacts/contacts';
import Fields from './fields/fields';

class Info extends Component {
    constructor(props){
        super(props);
        this.state = {};
    }
    componentDidMount() {

    }
    render() {
        let info = this.props.info;
        let params = this.props.params;
        let infoStyle = {
            width: '100%',
        };
        let infoStatusStyle = {
            paddingTop: '20px',
            display: 'flex',
            flexDirection: 'row',
        };
        let infoStatusTitleStyle = {
            fontWeight: '900',
            fontSize: '12px',
            color: '#ffffff',
        };
        let infoStatusTextStyle = {
            paddingLeft: '5px',
            fontSize: '12px',
            color: '#4CAF50',
        };
        let infoContactsStyle = {
            paddingTop: '20px',
        };
        let infoFieldsStyle = {

        };
        let contacts = info.detail.contacts.map((contact)=>{
            return <Contacts key={contact.phone} name={contact.name} phone={contact.phone}/>;
        });
        let fields = params.params[0].detail.info.map((field)=>{
            return info.detail.info.map((text)=>{
                if (field.id === text.id) {
                    return <Fields key={field.id} name={field.name} text={text.text}/>;
                }
            });
            
        });
        return (
        <div style={infoStyle}>

            <div style={infoStatusStyle}>
                <div style={infoStatusTitleStyle}>Статус:</div>
                <div style={infoStatusTextStyle}>{info.detail.status}</div>
            </div>

            <div style={infoContactsStyle}>
                {contacts}
            </div>

            <div style={infoFieldsStyle}>
                {fields}
            </div>
        </div>
        );
    }
}

const mapStateToProps = function(store) {
    return {
        menu: store.menu,
        btn: store.btn,
        options: store.options,
        params: store.params,
        detail: store.detail,
    };
};

export default connect(mapStateToProps)(Info);