import React, { Component } from 'react';

class Fields extends Component {
  constructor(props){
    super(props);
    this.state = {};
  }
  componentDidMount() {

  }
  render() {
    let generalStyle = {
        padding: '5px 0',
        color: '#ffffff',
        fontSize: '12px',
        borderBottom: '1px dotted #192227',
        display: 'flex',
    };
    let nameStyle = {
        width: '100px',
        fontWeight: '800',
    };
    let textStyle = {
        paddingLeft: '10px',
    };
    return (
    <div style={generalStyle}>
        <div style={nameStyle}>{this.props.name}</div>
        <div style={textStyle}>{this.props.text}</div>
    </div>
    );
  }
}

export default Fields;