import React, {Component} from 'react';
import store from './../../../../store/index';
import {connect} from "react-redux";
import FormsLookRoof from './formsLookRoof';
import FormsLookAprons from './formsLookAprons';
import FormsLookPerimeter from './formsLookPerimeter';

class FormsLook extends Component {
    constructor(props) {
        super(props);
        this.state = {
            photo: '',
            img: [],
            count: 'Выберите фотографии +',
            other: '',
        };
        this.dataPreparation = this.dataPreparation.bind(this);
        this.send = this.send.bind(this);
        this.photoSend = this.photoSend.bind(this);
        this.photoInputChange = this.photoInputChange.bind(this);
        this.otherInputChange = this.otherInputChange.bind(this);
    }
    dataPreparation(photoIds) {
        let obj = {};
        obj.work = {};
        this.props.application.work.map(el => {
            if (el === 'Кровля') obj.work.roof = this.props.form.formWork.roof;
            if (el === 'Козырьки') obj.work.aprons = this.props.form.formWork.aprons;
            if (el === 'Периметр') obj.work.perimeter = this.props.form.formWork.perimeter;
            return el;
        });
        obj.photo = photoIds;
        obj.other = this.state.other;
        return obj;
    }
    send() {
        this.photoSend(photoIds => {
            let data = this.dataPreparation(photoIds);
            console.log(data);
            fetch('/performer/applications/complete/' + this.props.application.id, {method: 'POST', headers: {'Accept': 'application/json', 'Content-Type': 'application/json'}, body: JSON.stringify(data)})
                .then(res => res.json())
                .then(resJson => {
                    console.log(resJson);
                    store.dispatch({type: 'BTN_STATE', btnState: 'menu'});
                    store.dispatch({type: 'DETAIL', detail: '-100%'});
                });
        });
    }
    photoSend(callback) {
        let photoFormData = new FormData();
        for (let x = 0; x < this.state.photo.length; x++) photoFormData.append('photo', this.state.photo[x]);
        fetch('/performer/applications/complete/' + this.props.application.id + '/photo', {method: 'POST', body: photoFormData})
            .then(res => res.json())
            .then(resJson => {
                console.log(resJson);
                callback(resJson.data);
            });
    }
    photoInputChange(e) {
        let labelText = e.target.files.length !== 0 ? 'Выбрано фотографий: ' + e.target.files.length : 'Выберите фотографии +';
        this.setState({photo: e.target.files, count: labelText});
    }
    otherInputChange(e) {this.setState({other: e.target.value})}
    componentDidMount() {console.log(this.props.application)}
    render() {
        let generalStyle = {
            paddingTop: '10px'
        };
        let btnStyle = {
            width: '100%',
            padding: '20px 0',
            margin: '20px 0',
            backgroundColor: '#4caf50',
            color: '#ffffff',
            textAlign: 'center',
            fontSize: '14px',
            fontWeight: '900',
            textTransform: 'uppercase',
            borderRadius: '10px',
            boxShadow: '0 0 5px -1px #000000',
        };
        let additionalContStyle = {
            marginBottom: '20px',
            padding: '10px',
            borderRadius: '10px',
            boxShadow: '0 0 5px -1px #000000',
            backgroundColor: 'rgba(255,255,255,0.1)'
        };
        const inputContStyle = {
            margin: '10px 0'
        };
        const inputLabelStyle = {
            fontSize: '12px'
        };
        const textInputStyle = {
            width: 'calc(100% - 10px)',
            borderRadius: '5px',
            padding: '5px',
            margin: '5px 0',
            outline: 'none',
            border: 'none',
        };
        let photoInputStyle = {
            width: '1px',
            height: '1px',
            overflow: 'hidden',
            opacity: '0',
        };
        let photoInputLabelStyle = {
            display: 'block',
            width: 'calc(100% - 10px)',
            padding: '6px 5px',
            fontSize: '14px',
            color: 'rgb(158, 158, 158)',
            marginTop: '-13px',
            backgroundColor: '#ffffff',
            borderRadius: '5px',
        };
        return (
            <div style={generalStyle}>
                <div>
                    {
                        this.props.application.work.map((el, i) => {
                            if (el === 'Кровля') return <FormsLookRoof key={i}/>;
                            if (el === 'Козырьки') return <FormsLookAprons key={i}/>;
                            if (el === 'Периметр') return <FormsLookPerimeter key={i}/>;
                            return null;
                        })
                    }
                </div>
                <div style={additionalContStyle}>
                    <div style={{margin: '15px 0'}}>
                        <div style={inputLabelStyle}>Фото</div>
                        <input style={photoInputStyle} type="file" name="photo" id="photo" onChange={this.photoInputChange} multiple/>
                        <label style={photoInputLabelStyle} htmlFor="photo">{this.state.count}</label>
                    </div>
                    <div style={inputContStyle}>
                        <div style={inputLabelStyle}>Примечания</div>
                        <input style={textInputStyle} onChange={this.otherInputChange} type="text"/>
                    </div>
                </div>
                <div onClick={this.send} style={btnStyle}>Отправить</div>
            </div>
        );
    }
}

const mapStateToProps = store => ({form: store.form});
export default connect(mapStateToProps)(FormsLook);