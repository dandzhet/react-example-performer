import React, { Component } from 'react';
import TimePicker from './timePicker/timePicker';

class Time extends Component {
        constructor(props) {
            super(props);
            this.state = {active: false, used: false, [this.props.id]: ''};
            this.focus = this.focus.bind(this);
            this.changeTime = this.changeTime.bind(this);
            this.displayTimePicker = 'none';
        }

        focus(e) {
            if (!this.state.used) {
                this.setState({used: true});
            }
            this.placeholderTransform = 'scale(0.6)';
            this.placejolderMarginLeft = '-10px';
            this.placejolderMarginTop = '-16px';
            this.borderWidth = '100%';
            this.displayTimePicker = 'flex';
            this.setState({active: true});
        }

        changeTime (value) {
            
            if (value) {            
                this.setState({[this.props.id]: value});

            } else {
                this.placeholderTransform = 'scale(1)';
                this.placejolderMarginLeft = '0px';
                this.placejolderMarginTop = '0px';
            }
            this.displayTimePicker = 'none';
            this.setState({active: false});
         }

      render() {
        let generalStyle = {
            position: 'relative',
            padding: '20px 0',
        };
        let formInputPlaceholder = {
            position: 'absolute',
            color: '#757575',
            transition: '.2s',
            transform: this.placeholderTransform,
            marginLeft: this.placejolderMarginLeft,
            marginTop: this.placejolderMarginTop,
        };
        let formInput = {
            width: '100%',
            position: 'relative',
            padding: '0',
            margin: '0',
            border: 'none',
            outline: 'none',
            background: 'transparent',
            color: '#ffffff',
        };
        let formBorder = {
            width: '100%',
            height: '2px',
            backgroundColor: '#757575',
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
        };
        let formBorderAfter = {
            position: 'absolute',
            width: '100%',
            content: 'Заполните поле',
            color: '#e63d32',
            fontSize: '10px',
            marginTop: '5px',
            textAlign: 'right',
            opacity: '0',
            transition: '.2s',   
        };


        return (
          <div style={generalStyle}>
            <TimePicker display={this.displayTimePicker} changeTime={this.changeTime} />
                <div>
                    <div style={formInputPlaceholder}>
                        {this.props.placeholder}
                    </div>
                    <input style={formInput} type={this.props.type} id={this.props.id} onFocus={this.focus} value={this.state[this.props.id]}/>
                </div>
                <div style={formBorder}>
                    <div style={{width: this.borderWidth}}></div>
                </div>
                <div style={formBorderAfter}>Заполните поле</div>               
          </div>
        );
      }
    }

export default Time;