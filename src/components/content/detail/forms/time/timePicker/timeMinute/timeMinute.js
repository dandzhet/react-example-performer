import React, { Component } from 'react';


class TimeMinute extends Component {
        constructor(props) {
            super(props);
            this.state = {};
        }
      render() {
        let numberStyle = {
            height: '40px',
            textAlign: 'center',
            lineHeight: '40px',
            transition: 'all .2s',
            fontSize: '20px',
            color: '#ffffff',
            opacity: this.props.opacity,
        }
        return (
            <div style = {numberStyle} data-minute={this.props.value}>{this.props.value}</div>
        );
      }
    }

export default TimeMinute;