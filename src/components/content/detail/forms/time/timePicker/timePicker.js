import React, { Component } from 'react';
import TimeHour from './timeHour/timeHour';
import TimeMinute from './timeMinute/timeMinute';

class TimePicker extends Component {
        constructor(props) {
            super(props);
            this.state = {active: false, scrollYes: false};
            this.valueBefore = null;

            this.selectedHour = '0';
            this.selectedMinute = '0';
            this.displayPicker = 'none';
            this.placeholderTransform = 'scale(0.6)';
            this.placejolderMarginLeft = '-10px';
            this.placejolderMarginTop = '-16px';

            this.hoursFunc = this.hoursFunc.bind(this);
            this.minutesFunc = this.minutesFunc.bind(this);
            this.cancelBtnFunc = this.cancelBtnFunc.bind(this);
            this.okBtnFunc = this.okBtnFunc.bind(this);
            this.hoursScrollEnd = this.hoursScrollEnd.bind(this);
            this.hoursScroll = this.hoursScroll.bind(this);
            this.minutesScrollEnd = this.minutesScrollEnd.bind(this);
            this.minutesScroll = this.minutesScroll.bind(this);
            this.scrollStartH = false;
            this.touchstartH = false;
            this.scrollStartM = false;
            this.touchstartM = false;
            this.differenceH = 40;
            this.relationsHours = [];
            this.differenceM = 40;
            this.relationsMinutes = [];
        }
        okBtnFunc() {
            let value;
            if (this.selectedMinute < 10) {
                value = this.selectedHour + ':' + '0' + this.selectedMinute;
            } else {
                value = this.selectedHour + ':' + this.selectedMinute;
            }
            
            this.valueBefore = value;
            console.log(value);  
            this.setState({scrollYes: false});
            this.setState({active:false});        
            this.props.changeTime(value);          
        }
        cancelBtnFunc() {
            this.displayPicker = 'none';
            if (this.valueBefore === null) {
                this.placeholderTransform = 'scale(1)';
                this.placejolderMarginLeft = '0px';
                this.placejolderMarginTop = '0px';
            }
            this.setState({scrollYes: false});
            this.setState({active:false});
            this.props.changeTime(this.valueBefore);
        }
        
        hoursFunc() {
            if (this.props.display !== 'none') {
                this.hoursCont = document.getElementById('hours');
                let hours = 24;
                for (let i = 0; i < hours; i++) {
                    let tempArr = [];
                    tempArr.push(i);
                    tempArr.push(this.differenceH);
                    this.relationsHours.push(tempArr);
                    this.differenceH += 40;
                }
                
                clearTimeout(this.initTimerH);
                this.initTimerH = setTimeout(() => {
                    this.hoursScrollEnd();
                }, 100);
                window.addEventListener('touchstart',() => {
                    this.touchstartH = true;
                });
                window.addEventListener('touchend',() => {
                    this.touchstartH = false;
                    this.hoursScrollEnd();
                });
                window.addEventListener('touchmove',() => {
                    this.setState({scrollYes:true});
                    clearTimeout(this.touchmoveTimerH);
                    this.touchstartH = false;
                    if (!this.scrollStartH) {
                        this.touchmoveTimerH = setTimeout(() => {
                            
                            this.hoursScrollEnd();
                        }, 20);
                    }
                });
            }
        }

        hoursScrollEnd () {
            this.setState({active:true});
            this.setState({scrollYes:false});
            if(this.hoursCont.scrollTop < this.relationsHours[0][1]) {
                this.hoursCont.scrollTop = this.relationsHours[0][1];
                this.selectedHour = this.relationsHours[0][0]
            } else if (this.hoursCont.scrollTop > this.relationsHours[this.relationsHours.length - 1][1]) {
                this.hoursCont.scrollTop = this.relationsHours[this.relationsHours.length - 1][1];
                this.selectedHour = this.relationsHours[this.relationsHours.length - 1][0];
            } else {
                this.relationsHours.forEach((el, i) => {
                    if (this.hoursCont.scrollTop < el[1] + 20 && this.hoursCont.scrollTop >= el[1] - 20) {
                        this.hoursCont.scrollTop = el[1];
                        this.selectedHour = el[0];
                    }
                });
            }
        }
    
        hoursScroll() {
            this.setState({scrollYes:true});
            clearTimeout(this.scrollTimerH);
                this.scrollStartH = true;
                this.scrollTimerH = setTimeout(() => {
                    this.scrollStartH = false;
                    if (!this.touchstartH) {
                        this.hoursScrollEnd();
                        
                    }
                }, 100);
        }

        minutesScroll() {
            this.setState({scrollYes:true});
            clearTimeout(this.scrollTimerM);
                this.scrollStartM = true;
                this.scrollTimerM = setTimeout(() => {
                    this.scrollStartM = false;
                    if (!this.touchstartM) {
                        this.minutesScrollEnd();
                    }
                }, 100); 
        }

        minutesFunc() {
            if (this.props.display !== 'none') {
            this.minutesCont = document.getElementById('minutes');        
            let minutes = 60;
            for (let i = 0; i < minutes; i++) {
                let tempArr = [];
                tempArr.push(i);
                tempArr.push(this.differenceM);
                this.relationsMinutes.push(tempArr);
                this.differenceM += 40;
            }
            
            clearTimeout(this.initTimerM);
            
            this.initTimerM = setTimeout(() => {
                this.minutesScrollEnd();
            }, 100);
            window.addEventListener('touchstart',() => {
                this.touchstartM = true;
            });
            window.addEventListener('touchend',() => {
                this.touchstartM = false;
                this.minutesScrollEnd();
            });
            window.addEventListener('touchmove',() => {
                this.setState({scrollYes:true});
                clearTimeout(this.touchmoveTimerM);
                this.touchstartM = false;
                if (!this.scrollStartM) {
                    this.touchmoveTimerM = setTimeout(() => {
                        this.minutesScrollEnd();
                    }, 20);
                }
            });
        }
        }
        minutesScrollEnd = () => {
            this.setState({scrollYes:false});
            if(this.minutesCont.scrollTop < this.relationsMinutes[0][1]) {
                this.minutesCont.scrollTop = this.relationsMinutes[0][1];
                this.selectedMinute = this.relationsMinutes[0][0];
            } else if (this.minutesCont.scrollTop > this.relationsMinutes[this.relationsMinutes.length - 1][1]) {
                this.minutesCont.scrollTop = this.relationsMinutes[this.relationsMinutes.length - 1][1];
                this.selectedMinute = this.relationsMinutes[this.relationsMinutes.length - 1][0];
            } else {
                this.relationsMinutes.forEach((el, i) => {
                    if (this.minutesCont.scrollTop < el[1] + 20 && this.minutesCont.scrollTop >= el[1] - 20) {
                        this.minutesCont.scrollTop = el[1];
                        this.selectedMinute = el[0];
                    }
                });
            }
        }
      render() {
        let timePickerStyle = {
            position: 'absolute',
            zIndex: '2',
            width: '100vw',
            height: '100vh',
            display: this.displayPicker,
            flexDirection: 'column',
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: 'rgba(0, 0, 0, 0.5)',
            left: '-10px',
            top: '-50vh', //???
        };
        let timePickerWrapStyle = {
            width: 'calc(100% - 40px)',
            boxShadow: '0 0 5px -1px #000000',
            backgroundColor: '#232c31',
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'center',
            alignItems: 'center',
            position: this.WrapPosition,
        };
        let timePickerBlockStyle = {
            width: '100%',
            height: '100px',
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
        }
        let timePickerBtns = {
            width: '100%',
            display: 'flex',
            flexDirection: 'row',
            alignItems: 'flex-end',
            justifyContent: 'space-between',
            backgroundColor: '#192227',
        }
        let timePickerBtnsStyle = {
            color: '#ffffff',
            fontSize: '30px',
            width: '50%',
            padding: '30px 0',
            textAlign: 'center',
            position: 'relative'
        }  
        let cancelBtn1 = {
            background: '#ffffff',
            height: '40px', 
            width: '10px',
            position: 'absolute',
            top: '0',
            bottom: '0',
            left: '0',
            right: '0',
            margin: 'auto',
            transform: 'rotate(45deg)',
            borderRadius: '2px',
        }       
        let cancelBtn2 = {
            height: '10px', 
            width: '40px',
            background: '#ffffff',
            position: 'absolute',
            top:'0',
            bottom: '0',
            left: '0',
            right: '0',
            margin: 'auto',
            transform: 'rotate(45deg)',
            borderRadius: '2px',
            
        }
        let okBtn1 = {
            height: '40px', 
            width: '10px',
            background: '#ffffff',
            position: 'absolute',
            top:'0',
            bottom: '0',
            left: '5px',
            right: '0',
            margin: 'auto',
            transform: 'rotate(45deg)',
            borderRadius: '2px',
        }
        let okBtn2 = {
            height: '10px', 
            width: '20px',
            background: '#ffffff',
            position: 'absolute',
            top:'22px',
            left: '-1px',
            transform: 'rotate(45deg)',
            borderRadius: '2px',
        }
        let timePickerHoursStyle = {
            height: '100px',
            width: 'calc(50% - 1px)',
            overflow: 'auto',
            scrollBehavior: 'smooth',
        }
        let timePickerHoursItems = {
            padding: '70px 0',
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'center',
            alignItems: 'center',
        }
        let timePickerSeparator = {
            width: '2px',
            height: '100px',
            backgroundColor: '#192227',
        }
        let timePickerOkStyle = {
            height: '40px',
            width: '40px',
            top:'0',
            bottom: '0',
            left: '0',
            right: '0',
            margin: 'auto',
            position: 'absolute',
            overflow: 'hidden'
        }
        if (this.state.active === false){
            this.displayPicker = this.props.display;
        }

        let N = 24; 
        this.houres = Array.apply(null, {length: N}).map(Number.call, Number);
        this.renderHour = this.houres.map((i) => {
            if (this.selectedHour !== i){
                return (
                    <TimeHour value={i} opacity={'0.2'} key={'h'+i}/>
                )
            }else{
                return (
                    <TimeHour value={i} opacity={'1'} key={'h'+i}/>
                )
            }
        });
        N = 60; 
        let minute = Array.apply(null, {length: N}).map(Number.call, Number);
        this.renderMinute = minute.map((i) => {
            if (this.selectedMinute !== i){
                if (i < 10) {
                    return (
                        <TimeMinute value={'0' + i} opacity={'0.2'} key={'m'+i}/>
                    )
                } else {
                    return (
                        <TimeMinute value={i} opacity={'0.2'} key={'m'+i}/>
                    )
                }
            }else{
                if (i < 10) {
                    return (
                        <TimeMinute value={'0' + i} opacity={'1'} key={'m'+i}/>
                    )
                } else {
                    return (
                        <TimeMinute value={i} opacity={'1'} key={'m'+i}/>
                    )
                }
            }
        });

        this.minutesFunc();
        this.hoursFunc();
        return (
          <div style = {timePickerStyle}>
                   <div style = {timePickerWrapStyle}>
                        <div style = {timePickerBlockStyle}>
                        <div style = {timePickerHoursStyle} onScroll = {this.hoursScroll} id = 'hours' >
                            <div style = {timePickerHoursItems}>
                                {this.renderHour}
                            </div>
                            <div style = {timePickerSeparator}></div>
                        </div>
                        <div style = {timePickerHoursStyle} onScroll = {this.minutesScroll} id = 'minutes' >
                            <div style = {timePickerHoursItems}>
                                {this.renderMinute}
                            </div>
                        </div>
                        </div>
                        <div style = {timePickerBtns}>
                            <div style = {timePickerBtnsStyle} onClick={this.cancelBtnFunc}>
                             <div style= {timePickerOkStyle}>
                                <div style = {cancelBtn1}></div>
                                <div style = {cancelBtn2}></div>
                            </div>
                            </div>
                            <div style = {timePickerBtnsStyle} onClick={this.okBtnFunc}>
                            <div style= {timePickerOkStyle}>
                                <div style = {okBtn1}></div> 
                                <div style = {okBtn2}></div>
                            </div>
                            </div>
                        </div>
                    </div> 
                </div>

        );
      }
    }

export default TimePicker;