import React, {Component} from 'react';
import store from './../../../../store/index';
import {connect} from "react-redux";

class FormsLookRoof extends Component {
    constructor(props) {
        super(props);
        this.state = {};
        this.inputChange = this.inputChange.bind(this);
    }
    inputChange(e, name) {
        let workData = this.props.form.formWork;
        workData.roof[name] = e.target.value;
        store.dispatch({type: 'FORM_WORK', formWork: workData});
    }
    render() {
        const WrapStyle = {
            marginBottom: '20px',
            padding: '10px',
            borderRadius: '10px',
            boxShadow: '0 0 5px -1px #000000',
            backgroundColor: 'rgba(255,255,255,0.1)'
        };
        const TitleStyle = {
            fontWeight: '900',
        };
        const inputContStyle = {
            margin: '10px 0'
        };
        const inputLabelStyle = {
            fontSize: '12px'
        };
        const textInputStyle = {
            width: 'calc(100% - 10px)',
            borderRadius: '5px',
            padding: '5px',
            margin: '5px 0',
            outline: 'none',
            border: 'none',
        };
        const selectInputStyle = {
            color: '#212121',
            backgroundColor: '#fff',
            width: '100%',
            borderRadius: '5px',
            outline: 'none',
            border: 'none',
            margin: '5px 0',
            padding: '4px 0',
        };
        return (
            <div style={WrapStyle}>
                <div style={TitleStyle}>Кровля</div>
                <div style={inputContStyle}>
                    <div style={inputLabelStyle}>Площадь (м2)</div>
                    <input style={textInputStyle} onChange={e => this.inputChange(e, 'area')} type="number"/>
                </div>
                <div style={inputContStyle}>
                    <div style={inputLabelStyle}>Высота снежного покрова (см)</div>
                    <input style={textInputStyle} onChange={e => this.inputChange(e, 'height')} type="number"/>
                </div>
                <div style={inputContStyle}>
                    <div style={inputLabelStyle}>Тип кровли</div>
                    <select style={selectInputStyle} onChange={e => this.inputChange(e, 'roofType')}>
                        <option value="pitched">Скатная</option>
                        <option value="flat">Плоская</option>
                    </select>
                </div>
            </div>
        );
    }
}

const mapStateToProps = store => ({form: store.form});
export default connect(mapStateToProps)(FormsLookRoof);