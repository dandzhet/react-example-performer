import React, { Component } from 'react';
import store from './../../../../../store/index';
import {connect} from "react-redux";

class Number extends Component {
    constructor(props) {
        super(props);
        this.state = {active: false, used: false, [this.props.id]: '', borderWidth: '0'};
        this.change = this.change.bind(this);
        this.focus = this.focus.bind(this);
        this.blur = this.blur.bind(this);
    }
    change(e) {
        let value = e.target.value;
        this.setState({[this.props.id]: value});
        store.dispatch({
            type: 'FORMS',
            forms: {id: this.props.id, value: value}
        });
        //console.log(this.props.forms);
        //console.log(this.state[this.props.id]);
    }
    focus() {
        if (!this.state.used) {
            this.setState({used: true});
        }
        this.setState({active: true});
        this.placeholderTransform = '8px';
        this.placeholderMarginTop = '-14px';
        this.setState({borderWidth: '100%'});
    }
    blur(e) {
        this.setState({active: false});
        let val = e.target.value;
        if (!val) {
            this.placeholderTransform = '12px';
            this.placeholderMarginTop = '0';
        }
        this.setState({borderWidth: '0'});
    }
    render() {
        let generalStyle = {
            position: 'relative',
            padding: '20px 0',
            fontSize: '12px'
        };
        let formInputPlaceholder = {
            position: 'absolute',
            marginTop: this.placeholderMarginTop,
            color: '#757575',
            transition: '.2s',
            fontSize: this.placeholderTransform,
        };
        let formInput = {
            width: '100%',
            position: 'relative',
            padding: '0',
            margin: '0',
            border: 'none',
            outline: 'none',
            background: 'transparent',
            color: '#ffffff',
        };
        let formBorderStyle = {
            width: '100%',
            height: '2px',
            marginTop: '6px',
            backgroundColor: '#757575',
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
        };
        let formBorderDivStyle = {
            width: this.state.borderWidth,
            height: '2px',
            backgroundColor: '#ffffff',
            transition: '.2s',
        };
        let formBorderAfterStyle = {
            position: 'absolute',
            width: '100%',
            content: 'Заполните поле',
            color: '#e63d32',
            fontSize: '10px',
            marginTop: '5px',
            textAlign: 'right',
            opacity: '0',
            transition: '.2s',
        };
        return (
            <div style={generalStyle}>
                <div>
                    <div style={formInputPlaceholder}>
                        {this.props.placeholder}
                    </div>
                    <input style={formInput} type={'number'} onFocus={this.focus} onBlur={this.blur} value={this.state[this.props.id]} onChange={this.change}/>
                </div>
                <div style={formBorderStyle}>
                    <div style={formBorderDivStyle}> </div>
                </div>
                <div style={formBorderAfterStyle}>Заполните поле</div>
            </div>
        );
    }
}

const mapStateToProps = function(store) {
    return {
        forms: store.forms
    };
};

export default connect(mapStateToProps)(Number);