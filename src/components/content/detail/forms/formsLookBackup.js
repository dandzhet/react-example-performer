import React, {Component} from 'react';
import Text from './text/text';
import store from './../../../../store/index';
import {connect} from "react-redux";

class FormsLook extends Component {
    constructor(props) {
        super(props);
        this.state = {
            photo: '',
            img: [],
            count: 'Выберите фотографии +',
        };
        this.workValueChange = this.workValueChange.bind(this);
        this.send = this.send.bind(this);
        this.photoInputChange = this.photoInputChange.bind(this);
    }
    workValueChange(e, id) {
        let work = this.props.form.work;
        let newWork = work.map(workEl => {
            workEl.items = workEl.items.map(itemsEl => {
                if (itemsEl.id.toString() === id.toString()) itemsEl.value = e.target.value;
                return itemsEl;
            });
            return workEl;
        });
        store.dispatch({type: 'FORM_WORK', work: newWork});
    }
    send() {
        let formData = new FormData();
        let len = this.state.photo.length;
        for (let x = 0; x < len; x++) formData.append('photo', this.state.photo[x]);
        formData.append('data', JSON.stringify({applicationReportId: this.props.applicationReportId, work: this.props.form.work, stage: 'look', other: this.props.form.additionally}));
        let xhr = new XMLHttpRequest();
        xhr.open("POST", "/performer/applications/report");
        xhr.send(formData);
        xhr.onload = () => {
            if (xhr.status === 200) {
                console.log(JSON.parse(xhr.response));
                store.dispatch({type: 'BTN_STATE', btnState: 'menu'});
                store.dispatch({type: 'DETAIL', detail: '-100%'});
            }
            else console.log('err');
        };
        xhr.onerror = () => console.log('err');
    }
    photoInputChange(e) {
        let labelText = e.target.files.length !== 0 ? 'Выбрано фотографий: ' + e.target.files.length : 'Выберите фотографии +';
        this.setState({photo: e.target.files, count: labelText});
    }
    componentDidMount() {
        let workForStore = this.props.work.map(workEl => ({id: workEl.id, items: workEl.items.map(itemsEl => ({id: itemsEl.id, value: ''}))}));
        store.dispatch({type: 'FORM_WORK', work: workForStore});
    }
    render() {
        let generalStyle = {
            paddingTop: '10px'
        };
        let btnStyle = {
            width: '100%',
            padding: '20px 0',
            margin: '40px 0',
            backgroundColor: '#4caf50',
            color: '#ffffff',
            textAlign: 'center',
            fontSize: '14px',
            fontWeight: '900',
            textTransform: 'uppercase',
        };
        let photoInputStyle = {
            width: '1px',
            height: '1px',
            overflow: 'hidden',
            opacity: '0',
        };
        let photoInputLabelStyle = {
            display: 'block',
            width: '100%',
            padding: '5px 0',
            fontSize: '14px',
            color: 'rgb(158, 158, 158)',
            borderBottom: '2px solid',
        };
        return (
            <div style={generalStyle}>
                <div style={{backgroundColor: 'rgba(0,0,0,0.2)', padding: '10px'}}>
                    <div style={{marginBottom: '10px'}}>Укажите объем работ</div>
                    {/*
                        this.props.work.map((workEl, workElI) => {
                            return (
                                <div key={workElI}>
                                    <div style={{color: '#eeeeee', fontSize: '14px'}}>{workEl.name}</div>
                                    <div style={{marginTop: '10px'}}>
                                        {
                                            workEl.items.map((itemsEl, itemsElI) => {
                                                return (
                                                    <div key={itemsElI}>
                                                        <div style={{color: '#eeeeee', fontSize: '12px'}}>{itemsEl.name} ({itemsEl.unit})</div>
                                                        <input onChange={e => this.workValueChange(e, itemsEl.id)} defaultValue='0' type='number'/>
                                                    </div>
                                                );
                                            })
                                        }
                                    </div>
                                </div>
                            )
                        })
                    */}
                </div>
                <div>
                    <input style={photoInputStyle} type="file" name="photo" id="photo" onChange={this.photoInputChange} multiple/>
                    <label style={photoInputLabelStyle} htmlFor="photo">{this.state.count}</label>
                </div>
                <Text placeholder={'Дополнительная информация'} id={'additionally'} forStore={'FORM_ADDITIONALLY'}/>
                <div onClick={this.send} style={btnStyle}>Отправить</div>
            </div>
        );
    }
}

const mapStateToProps = store => ({detail: store.detail, form: store.form});
export default connect(mapStateToProps)(FormsLook);