import React, {Component} from 'react';
import store from './../../../../../store/index';
import {connect} from "react-redux";

class Select extends Component {
    constructor(props) {
        super(props);
        this.state = {
            active: false,
            used: false,
            items: [],
            selected: '',
            placeholderMarginTop: '0',
            placeholderFontSize: '12px',
            itemsDisplay: 'none',
            itemsOpacity: '0',
            borderWidth: '0',
        };
        this.openType = this.openType.bind(this);
        this.choseType = this.choseType.bind(this);
    }

    componentDidMount() {
        let itemsProps = this.props.items;
        itemsProps.unshift('- - -');
        this.setState({items: itemsProps});
        document.addEventListener('click', () => {
            if (this.state.active) {
                if (this.state.selected === '') {
                    this.setState({
                        placeholderMarginTop: '0',
                        placeholderFontSize: '12px',
                    });
                }
                this.setState({
                    itemsOpacity: '0',
                    borderWidth: '0'
                });
                setTimeout(() => {
                    this.setState({itemsDisplay: 'none'});
                    this.setState({active: false});
                }, 200);
            }
        });
    }

    openType() {
        if (!this.state.used) {
            this.setState({used: true});
        }
        if (!this.state.active) {
            setTimeout(() => {
                this.setState({
                    active: true,
                    placeholderMarginTop: '-14px',
                    placeholderFontSize: '8px',
                    itemsDisplay: 'flex',
                    borderWidth: '100%'
                });
                setTimeout(() => {
                    this.setState({itemsOpacity: '1'});
                },4);
            },4);
        } else {
            this.setState({
                active: false,
                placeholderMarginTop: '0',
                placeholderFontSize: '12px',
                itemsOpacity: '0',
                borderWidth: '0'
            });
            setTimeout(() => {
                this.setState({itemsDisplay: 'none'});
            },200);
        }
    }

    choseType(e) {
        let value = e.target.innerHTML;
        if (value === this.state.items[0]) {
            this.setState({
                active: false,
                placeholderMarginTop: '0',
                placeholderFontSize: '12px',
                selected: '',
                itemsOpacity: '0',
                borderWidth: '0',
            });
        } else {
            this.setState({active: false, selected: value, itemsOpacity: '0', borderWidth: '0'});
        }
        this.setState({itemsOpacity: '0', borderWidth: '0'});
        setTimeout(()=>{
            this.setState({itemsDisplay: 'none'});
        },200);
        store.dispatch({
            type: 'FORMS',
            forms: {id: this.props.id, value: value}
        });
    }

    render() {
        let generalStyle = {
            position: 'relative',
            padding: '20px 0',
            fontSize: '12px',
        };
        let formInputPlaceholderStyle = {
            position: 'absolute',
            marginTop: this.state.placeholderMarginTop,
            color: '#757575',
            transition: '.2s',
            fontSize: this.state.placeholderFontSize,
        };
        let inputStyle = {
            position: 'relative',
        };
        let valueStyle = {
            minHeight: '12px',
            color: '#ffffff',
        };
        let formTypeWorkStyle = {
            position: 'absolute',
            zIndex: '1',
            opacity: this.state.itemsOpacity,
            marginTop: '10px',
            width: '100%',
            backgroundColor: '#ffffff',
            borderRadius: '3px',
            color: '#212121',
            transition: '.2s',
            display: this.state.itemsDisplay,
            flexDirection: 'column',
            alignItems: 'flex-start',
            justifyContent: 'center',
        };
        let formBorderStyle = {
            width: '100%',
            height: '2px',
            marginTop: '6px',
            backgroundColor: '#757575',
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
        };
        let formBorderDivStyle = {
            width: this.state.borderWidth,
            height: '2px',
            backgroundColor: '#ffffff',
            transition: '.2s',
        };
        let typeWorkItemStyle = {
            width: '100%',
            padding: '15px',
            fontSize: '14px',
            display: 'flex',
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
        };
        let formBorderAfterStyle = {
            position: 'absolute',
            width: '100%',
            content: 'Заполните поле',
            color: '#e63d32',
            fontSize: '10px',
            marginTop: '5px',
            textAlign: 'right',
            opacity: '0',
            transition: '.2s',
        };
        let items = this.state.items.map((item) => {
            return (
                <div key={item} style={typeWorkItemStyle} onClick={this.choseType}>{item}</div>
            )
        });
        return (
            <div style={generalStyle}>
                <div>
                    <div style={formInputPlaceholderStyle}>
                        {this.props.placeholder}
                    </div>
                    <div style={inputStyle}>
                        <div style={valueStyle} onClick={this.openType}>{this.state.selected}</div>
                        <div style={formTypeWorkStyle}>
                            {items}
                        </div>
                    </div>
                </div>
                <div style={formBorderStyle}>
                    <div style={formBorderDivStyle}> </div>
                </div>
                <div style={formBorderAfterStyle}>Заполните поле</div>
            </div>
        );
    }
}

const mapStateToProps = function(store) {
    return {
        forms: store.forms
    };
};

export default connect(mapStateToProps)(Select);