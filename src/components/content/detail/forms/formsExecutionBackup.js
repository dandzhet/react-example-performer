import React, {Component} from 'react';
import Text from './text/text';
import store from './../../../../store/index';
import {connect} from "react-redux";

class FormsExecution extends Component {
    constructor(props) {
        super(props);
        this.state = {
            beforePhoto: '',
            beforeImg: [],
            beforeCount: 'Фотографии до +',
            afterPhoto: '',
            afterImg: [],
            afterCount: 'Фотографии после +',
        };
        this.send = this.send.bind(this);
        this.photoSend = this.photoSend.bind(this);
        this.photoBeforeInputChange = this.photoBeforeInputChange.bind(this);
        this.photoAfterInputChange = this.photoAfterInputChange.bind(this);
    }
    send() {
        //store.dispatch({type: 'BTN_STATE', btnState: 'menu'});
        //store.dispatch({type: 'DETAIL', detail: '-100%'});
    }
    photoSend() {
        let formData = new FormData();
        let beforeLen = this.state.beforePhoto.length;
        for (let x = 0; x < beforeLen; x++) formData.append('photoBefore', this.state.beforePhoto[x]);
        let afterLen = this.state.afterPhoto.length;
        for (let y = 0; y < afterLen; y++) formData.append('photoAfter', this.state.afterPhoto[y]);
    }
    photoBeforeInputChange(e) {
        let labelText;
        if (e.target.files.length !== 0) labelText = 'Выбрано фотографий: ' + e.target.files.length;
        else labelText = 'Фотографии до +';
        this.setState({beforePhoto: e.target.files, beforeCount: labelText});
    }
    photoAfterInputChange(e) {
        let labelText;
        if (e.target.files.length !== 0) labelText = 'Выбрано фотографий: ' + e.target.files.length;
        else labelText = 'Фотографии после +';
        this.setState({afterPhoto: e.target.files, afterCount: labelText});
    }
    componentDidMount() {console.log(this.props.application)}
    render() {
        /*let generalStyle = {
            paddingTop: '10px'
        };
        let btnStyle = {
            width: '100%',
            padding: '20px 0',
            margin: '20px 0',
            backgroundColor: '#4caf50',
            color: '#ffffff',
            textAlign: 'center',
            fontSize: '14px',
            fontWeight: '900',
            textTransform: 'uppercase',
            borderRadius: '10px',
        };
        let photoInputStyle = {
            width: '1px',
            height: '1px',
            overflow: 'hidden',
            opacity: '0',
        };
        let photoInputLabelStyle = {
            display: 'block',
            width: '100%',
            padding: '5px 0',
            fontSize: '14px',
            color: 'rgb(158, 158, 158)',
            borderBottom: '2px solid',
        };*/

        let generalStyle = {
            paddingTop: '10px'
        };
        let btnStyle = {
            width: '100%',
            padding: '20px 0',
            margin: '20px 0',
            backgroundColor: '#4caf50',
            color: '#ffffff',
            textAlign: 'center',
            fontSize: '14px',
            fontWeight: '900',
            textTransform: 'uppercase',
            borderRadius: '10px',
            boxShadow: '0 0 5px -1px #000000',
        };
        let additionalContStyle = {
            marginBottom: '20px',
            padding: '10px',
            borderRadius: '10px',
            boxShadow: '0 0 5px -1px #000000',
            backgroundColor: 'rgba(255,255,255,0.1)'
        };
        const inputContStyle = {
            margin: '10px 0'
        };
        const inputLabelStyle = {
            fontSize: '12px'
        };
        const textInputStyle = {
            width: 'calc(100% - 10px)',
            borderRadius: '5px',
            padding: '5px',
            margin: '5px 0',
            outline: 'none',
            border: 'none',
        };
        let photoInputStyle = {
            width: '1px',
            height: '1px',
            overflow: 'hidden',
            opacity: '0',
        };
        let photoInputLabelStyle = {
            display: 'block',
            width: 'calc(100% - 10px)',
            padding: '6px 5px',
            fontSize: '14px',
            color: 'rgb(158, 158, 158)',
            marginTop: '-13px',
            backgroundColor: '#ffffff',
            borderRadius: '5px',
        };
        return (
            <div style={generalStyle}>




                {/*<div>
                    <input style={photoInputStyle} type="file" name="photoBefore" id="photoBefore" onChange={this.photoBeforeInputChange} multiple/>
                    <label style={photoInputLabelStyle} htmlFor="photoBefore">{this.state.beforeCount}</label>
                </div>
                <div>
                    <input style={photoInputStyle} type="file" name="photoAfter" id="photoAfter" onChange={this.photoAfterInputChange} multiple/>
                    <label style={photoInputLabelStyle} htmlFor="photoAfter">{this.state.afterCount}</label>
                </div>
                <Text placeholder={'Дополнительная информация'} id={'additionally'} forStore={'FORM_ADDITIONALLY'}/>*/}


                <div style={additionalContStyle}>
                    <div style={{margin: '15px 0'}}>
                        <div style={inputLabelStyle}>Фото</div>
                        <input style={photoInputStyle} type="file" name="photo" id="photo" onChange={this.photoInputChange} multiple/>
                        <label style={photoInputLabelStyle} htmlFor="photo">{this.state.count}</label>
                    </div>
                    <div style={inputContStyle}>
                        <div style={inputLabelStyle}>Примечания</div>
                        <input style={textInputStyle} onChange={this.otherInputChange} type="text"/>
                    </div>
                </div>

                <div onClick={this.send} style={btnStyle}>Отправить</div>
            </div>
        );
    }
}

const mapStateToProps = store => ({detail: store.detail, form: store.form});
export default connect(mapStateToProps)(FormsExecution);