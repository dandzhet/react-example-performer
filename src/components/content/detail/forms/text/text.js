import React, {Component} from 'react';
import {connect} from 'react-redux';
import store from './../../../../../store/index';

/*
PROPS
-placeholder
-id
-forStore
*/

class Text extends Component {
    constructor(props) {
        super(props);
        this.state = {active: false, used: false, [this.props.id]: ''};
        this.change = this.change.bind(this);
        this.focus = this.focus.bind(this);
        this.blur = this.blur.bind(this);
        this.borderWidth = '0%';
    }

    change(e) {
        let value = e.target.value;
        this.setState({[this.props.id]: value});
    }

    focus() {
        if (!this.state.used) {
            this.setState({used: true});
        }
        this.setState({active: true});
        this.placeholderTransform = '8px';
        this.placeholderMarginTop = '-14px';
        this.borderWidth = '100%';
    }

    blur(e) {
        this.setState({active: false});
        let val = e.target.value;
        if (!val) {
            this.placeholderTransform = '14px';
            this.placeholderMarginTop = '0px';
        }
        this.borderWidth = '0%';
        store.dispatch({
            type: this.props.forStore,
            [this.props.id]: val
        });
    }

    render() {
        let styles = {
            general: {
                position: 'relative',
                padding: '20px 0',
                width: '100%',
                fontSize: '14px',
            },
            placeholder: {
                position: 'absolute',
                color: '#9E9E9E',
                transition: '.2s',
                fontSize: this.placeholderTransform,
                marginTop: this.placeholderMarginTop,
            },
            input: {
                width: '100%',
                position: 'relative',
                padding: '0',
                margin: '0',
                border: 'none',
                outline: 'none',
                background: 'transparent',
                color: '#ffffff',
            },
            border: {
                width: '100%',
                height: '2px',
                marginTop: '6px',
                backgroundColor: '#9E9E9E',
                display: 'flex',
                flexDirection: 'row',
                justifyContent: 'center',
                alignItems: 'center',
            },
            borderDiv: {
                width: this.borderWidth,
                height: '2px',
                backgroundColor: '#ffffff',
                transition: '.2s',
            },
            warning: {
                position: 'absolute',
                width: '100%',
                content: 'Заполните поле',
                color: '#e63d32',
                fontSize: '10px',
                marginTop: '5px',
                textAlign: 'right',
                opacity: '0',
                transition: '.2s',
            },
        };
        return (
            <div style={styles.general}>
                <div>
                    <div style={styles.placeholder}>
                        {this.props.placeholder}
                    </div>
                    <input style={styles.input} type={this.props.type} id={this.props.id} onFocus={this.focus} onBlur={this.blur}
                           value={this.state[this.props.id]} onChange={this.change}/>
                </div>
                <div style={styles.border}>
                    <div style={styles.borderDiv}> </div>
                </div>
                <div style={styles.warning}>Заполните поле</div>
            </div>
        );
    }
}

const mapStateToProps = function (store) {
    return {
        form: store.form
    };
};

export default connect(mapStateToProps)(Text);