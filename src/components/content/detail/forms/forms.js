import React, {Component} from 'react';
import Text from './text/text';
import store from './../../../../store/index';
import {connect} from "react-redux";

class Forms extends Component {
    constructor(props) {
        super(props);
        this.state = {photo: '', img: [], count: 'Выберите фотографии +'};
        this.send = this.send.bind(this);
        this.photoInputChange = this.photoInputChange.bind(this);
    }
    send() {
        let sendForm = () => {
            console.log('sendForm');
            let obj = this.props.form;
            obj.performerDataId = this.props.detail.detailData;
            obj.taskId = this.props.detail.detailInfo;
            let json = JSON.stringify(obj);
            return new Promise((resolve, reject) => {
                let xhr = new XMLHttpRequest();
                xhr.open('POST', '/performer/form');
                xhr.setRequestHeader('Content-Type', 'application/json');
                xhr.send(json);
                xhr.onload = () => {
                  if (xhr.status === 200) resolve(xhr.response);
                  else reject('err');
                };
                xhr.onerror = () => reject('err');
            });
        };
        let sendPhoto = () => {
            console.log('sendPhoto');
            let formData = new FormData();
            let len = this.state.photo.length;
            for (let x = 0; x < len; x++) {
                formData.append('photo', this.state.photo[x]);
            }
            formData.append('performerDataId', this.props.detail.detailData);
            return new Promise((resolve, reject) => {
                let xhr = new XMLHttpRequest();
                xhr.open("POST", "/performer/photo");
                xhr.send(formData);
                xhr.onload = () => {
                  if (xhr.status === 200) {
                    resolve(xhr.response);
                  } else {
                    reject('err');
                  }
                };
                xhr.onerror = function() {
                  reject('err');
                };
            });
        };
        Promise.all([ sendForm(), sendPhoto() ]).then(results => {
            store.dispatch({
                type: 'BTN_STATE',
                btnState: 'menu'
            });
            store.dispatch({
                type: 'DETAIL',
                detail: '-100%'
            });
        });
    }
    photoInputChange(e) {
        let labelText;
        if (e.target.files.length !== 0) {
            labelText = 'Выбрано фотографий: ' + e.target.files.length;
        }
        else {
            labelText = 'Выберите фотографии +';
        }
        this.setState({
            photo: e.target.files,
            count: labelText,
        });
    }
    render() {
        let generalStyle = {
            paddingTop: '10px'
        };
        let btnStyle = {
            width: '100%',
            padding: '20px 0',
            margin: '40px 0',
            backgroundColor: '#4caf50',
            color: '#ffffff',
            textAlign: 'center',
            fontSize: '14px',
            fontWeight: '900',
            textTransform: 'uppercase',
        };
        let photoInputStyle = {
            width: '1px',
            height: '1px',
            overflow: 'hidden',
            opacity: '0',
        };
        let photoInputLabelStyle = {
            display: 'block',
            width: '100%',
            padding: '5px 0',
            fontSize: '14px',
            color: 'rgb(158, 158, 158)',
            borderBottom: '2px solid',
        };
        return (
            <div style={generalStyle}>
                <Text placeholder={'Виды работ'} id={'workKind'} forStore={'FORM_WORK_KIND'}/>
                <Text placeholder={'Дополнительная информация'} id={'additionally'} forStore={'FORM_ADDITIONALLY'}/>

                <div>
                    <input style={photoInputStyle} type="file" name="photo" id="photo" onChange={this.photoInputChange} multiple/>
                    <label style={photoInputLabelStyle} htmlFor="photo">{this.state.count}</label>
                </div>

                <div onClick={this.send} style={btnStyle}>Отправить</div>

            </div>
        );
    }
}

const mapStateToProps = store => ({detail: store.detail, form: store.form});
export default connect(mapStateToProps)(Forms);