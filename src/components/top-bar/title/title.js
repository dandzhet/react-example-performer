import React, {Component} from 'react';
import {connect} from 'react-redux';
//import store from './../../../store/index';
import menuItems from './../../menu/menuItems';

class Title extends Component {
    constructor(props) {
        super(props);
        this.state = {option: '- - -'};
        this.getOption = this.getOption.bind(this);
    }
    getOption(nextProps) {
        let newOption;
        if (nextProps) newOption = nextProps.options.optionsSelected;
        else newOption = this.props.options.optionsSelected;
        let option;
        switch (newOption) {
            case 'all':
                option = 'Все'
                break;
            case 'inspection':
                option = 'Осмотр'
                break;
            case 'performance':
                option = 'Выполнение'
                break;
            case 'active':
                option = 'Активные'
                break;
            case 'history':
                option = 'История'
                break;
            default:
                option = '- - -'
                break;
        }
        this.setState({ option: option });
    }
    componentWillReceiveProps(nextProps) {
        this.getOption(nextProps);
    }
    componentDidMount() {
        this.getOption();
    }
    render() {
        let styles = {
            general: {
                display: 'flex',
                flexDirection: 'row',
                alignItems: 'center',
            },
            title: {
                color: '#ffffff',
                fontSize: '14px',
            },
            optionWrap: {
                display: 'flex',
                flexDirection: 'row',
                alignItems: 'center',
            },
            option: {
                color: '#757575',
                fontSize: '10px',
            },
            split: {
                width: '1px',
                height: '20px',
                margin: '0 10px',
                backgroundColor: '#757575',
            },
        };
        return (
            <div style={styles.general}>
                <div style={styles.title}>
                    {this.props.menu.item}
                </div>
                {this.props.menu.item === menuItems.new || this.props.menu.item === menuItems.my ? 
                    <div style={styles.optionWrap}>
                        <div style={styles.split}></div>
                        <div  style={styles.option}>
                            {this.state.option}
                        </div>
                    </div>
                :
                    null
                }
                
            </div>
        );
    }
}

const mapStateToProps = function (store) {
    return {
        menu: store.menu,
        btn: store.btn,
        detail: store.detail,
        options: store.options,
    };
};

export default connect(mapStateToProps)(Title);