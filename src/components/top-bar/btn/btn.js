import React, {Component} from 'react';
import {connect} from 'react-redux';
import store from './../../../store/index';

class Btn extends Component {
    constructor(props) {
        super(props);
        this.state = {
            burger1top: '10px',
            burger1transform: 'rotate(0deg)',
            burger2opacity: '1',
            burger3bottom: '10px',
            burger3transform: 'rotate(0deg)',
        };
        this.click = this.click.bind(this);
    }
    click() {
        if (this.props.btn.btnState === 'menu') {
            store.dispatch({
                type: 'MENU_OPEN',
                open: true
            });
            store.dispatch({
                type: 'BTN_STATE',
                btnState: 'close'
            });
        } else if (this.props.btn.btnState === 'close') {
            store.dispatch({
                type: 'MENU_OPEN',
                open: false
            });
            store.dispatch({
                type: 'BTN_STATE',
                btnState: 'menu'
            });
        } else if (this.props.btn.btnState === 'back') {
            store.dispatch({
                type: 'BTN_STATE',
                btnState: 'menu'
            });
            store.dispatch({
                type: 'DETAIL',
                detail: '-100%'
            });

        }
    }
    render() {
        let styles = {
            btn: {
                position: 'relative',
                width: '40px',
                height: '40px',
                margin: '10px',
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
                justifyContent: 'center',
            },
            burger: {
                position: 'absolute',
                width: '24px',
                height: '4px',
                backgroundColor: '#ffffff',
                transition: '.2s',
            },
            burger1: {
                width: this.props.btn.btnState === 'menu' ? '24px' :  this.props.btn.btnState === 'close' ? '24px' : '14px',
                top: this.props.btn.btnState === 'menu' ? '10px' :  this.props.btn.btnState === 'close' ? '18px' : '14px',
                left: this.props.btn.btnState === 'menu' ? ' ' :  this.props.btn.btnState === 'close' ? ' ' : '8px',
                transform: this.props.btn.btnState === 'menu' ? 'rotate(0deg)' :  this.props.btn.btnState === 'close' ? 'rotate(45deg)' : 'rotate(-45deg)',
            },
            burger2: {
                opacity: this.props.btn.btnState === 'menu' ? '1' :  '0',
            },
            burger3: {
                width: this.props.btn.btnState === 'menu' ? '24px' :  this.props.btn.btnState === 'close' ? '24px' : '14px',
                bottom: this.props.btn.btnState === 'menu' ? '10px' :  this.props.btn.btnState === 'close' ? '18px' : '14px',
                left: this.props.btn.btnState === 'menu' ? ' ' :  this.props.btn.btnState === 'close' ? ' ' : '8px',
                transform: this.props.btn.btnState === 'menu' ? 'rotate(0deg)' :  this.props.btn.btnState === 'close' ? 'rotate(-45deg)' : 'rotate(45deg)',
            },
        };
        return (
            <div style={styles.btn} onClick={this.click}>
                <div style={{...styles.burger,...styles.burger1}}> </div>
                <div style={{...styles.burger,...styles.burger2}}> </div>
                <div style={{...styles.burger,...styles.burger3}}> </div>
            </div>
        );
    }
}

const mapStateToProps = function (store) {
    return {
        menu: store.menu,
        btn: store.btn,
        detail: store.detail
    };
};

export default connect(mapStateToProps)(Btn);