import React, {Component} from 'react';
import {connect} from 'react-redux';
import store from './../../../store/index';
import './options.css';
import menuItems from './../../menu/menuItems';

class Options extends Component {
    constructor(props) {
        super(props);
        this.state = {
            open: false,
            styles: {
                opacity: '0',
                display: 'none'
            },
        };
        this.click = this.click.bind(this);
        this.optionClick = this.optionClick.bind(this);
    }
    click() {
        setTimeout(() => {
            this.setState({open: true});
            this.setState({styles: {display: 'flex', opacity: '0'}});
            setTimeout(() => {
                this.setState({styles: {display: 'flex', opacity: '1'}});
            }, 4);
        }, 4);
    }

    optionClick(e, item) {
        store.dispatch({
            type: 'OPTIONS_SELECT',
            optionsSelected: item,
        });
    }
    componentDidMount() {
        document.addEventListener('click', () => {
            if (this.state.open) {
                this.setState({styles: {display: 'flex', opacity: '0'}});
                setTimeout(() => {
                    this.setState({styles: {display: 'none', opacity: '0'}});
                    this.setState({open: false});
                }, 200);
            }
        });
    }
    render() {
        let optionsOpenStyle = {display: this.state.styles.display, opacity: this.state.styles.opacity};
        let optionsActiveItemDotStyle = {width: '6px', height: '6px', backgroundColor: '#212121', borderRadius: '6px'};
        let itemsMy = {
            active: 'active',
            history: 'history',
        };
        let itemsNew = {
            all: 'all',
            inspection: 'inspection',
            performance: 'performance',
        };
        let generalStyle = {
            position: 'relative',
            color: '#212121',
        };
        let spacerStyle = {
            width: '54px',
            height: '40px',
            background: 'transparent',
        };
        let spacer = (
            <div style={spacerStyle}></div>
        );
        let itemsMyDiv = (
            <div className="Options-items" style={optionsOpenStyle}>
                <div className="Options-item" onClick={(e) => {this.optionClick(e, itemsMy.active)}}>
                    <div>Активные</div>
                    {this.props.options.optionsSelected === itemsMy.active ? <div style={optionsActiveItemDotStyle}></div> : null}
                </div>
                <div className="Options-item" onClick={(e) => {this.optionClick(e, itemsMy.history)}}>
                    <div>История</div>
                    {this.props.options.optionsSelected === itemsMy.history ? <div style={optionsActiveItemDotStyle}></div> : null}
                </div>
            </div>
        );
        let itemsNewDiv = (
            <div className="Options-items" style={optionsOpenStyle}>
                <div className="Options-item" onClick={(e) => {this.optionClick(e, itemsNew.all)}}>
                    <div>Все</div>
                    {this.props.options.optionsSelected === itemsNew.all ? <div style={optionsActiveItemDotStyle}></div> : null}
                </div>
                <div className="Options-item" onClick={(e) => {this.optionClick(e, itemsNew.inspection)}}>
                    <div>Осмотр</div>
                    {this.props.options.optionsSelected === itemsNew.inspection ? <div style={optionsActiveItemDotStyle}></div> : null}
                </div>
                <div className="Options-item" onClick={(e) => {this.optionClick(e, itemsNew.performance)}}>
                    <div>Выполнение</div>
                    {this.props.options.optionsSelected === itemsNew.performance ? <div style={optionsActiveItemDotStyle}></div> : null}
                </div>
            </div>
        );
        let optionsBtn = (
            <div className="Options-btn" onClick={this.click}>
                <div className="Options-btn-dots"></div>
                <div className="Options-btn-dots"></div>
                <div className="Options-btn-dots"></div>
            </div>
        );
        return (
            <div style={generalStyle}>
                {this.props.menu.open ? spacer : this.props.menu.item === menuItems.new || this.props.menu.item === menuItems.my ? optionsBtn : spacer}
                {this.props.menu.item === menuItems.new ? itemsNewDiv : this.props.menu.item === menuItems.my ? itemsMyDiv : null}
            </div>
        );
    }
}

const mapStateToProps = function (store) {
    return {
        menu: store.menu,
        btn: store.btn,
        options: store.options
    };
};

export default connect(mapStateToProps)(Options);