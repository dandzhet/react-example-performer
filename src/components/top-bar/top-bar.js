import React, {Component} from 'react';
import Btn from './btn/btn';
import Title from './title/title';
import Options from './options/options';

class TopBar extends Component {
    render() {
        let topBarStyle = {
            position: 'relative',
            zIndex: '2',
            width: '100%',
            height: '60px',
            backgroundColor: '#192227',
            display: 'flex',
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
        };
        return (
            <div style={topBarStyle}>
                <Btn/>
                <Title/>
                <Options/>
            </div>
        );
    }
}

export default TopBar;