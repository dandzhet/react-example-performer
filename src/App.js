import React, {Component} from 'react';
import {connect} from 'react-redux';
import store from './store/index';
import Auth from './components/auth';
import TopBar from './components/top-bar/top-bar';
import Menu from './components/menu/menu';
import Content from './components/content/content';

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {};
        this.user = {};
    }

    getUser(callback) {
        let xhr = new XMLHttpRequest();
        xhr.open('POST', 'performer/user', true);
        xhr.send();
        xhr.onreadystatechange = () => {
            if (xhr.readyState !== 4) return;
            if (xhr.status !== 200) console.log(xhr.status + ': ' + xhr.statusText);
            else callback(JSON.parse(xhr.responseText));
        };
    }

    componentDidMount() {
        fetch('/performer/auth/check', {method: 'POST', headers: {'Accept': 'application/json', 'Content-Type': 'application/json'}})
            .then(res => {
                if (res.status !== 200) {
                    console.log('Err: ' + res.statusText + ' ' + res.status);
                    return;
                }
                res.json().then(data => {
                    if (data.status === 'ok') store.dispatch({type: 'AUTH', auth: true});
                });
            });
    }

    render() {
        let generalStyle = {
            position: 'relative',
            width: '100%',
            minHeight: '100vh',
            backgroundColor: '#232c31',
            overflowX: 'hidden',
        };
        return this.props.auth.auth ? (
            <div style={generalStyle}>
                <TopBar/>
                <Menu user={this.user}/>
                <Content/>
            </div>
        ) : <Auth/>
    }
}

const mapStateToProps = store => ({auth: store.auth, menu: store.menu});
export default connect(mapStateToProps)(App);