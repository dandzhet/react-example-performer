import { createStore, combineReducers } from 'redux'

const authReducer = (state = {auth: false}, action) => {
    switch (action.type) {
        case 'AUTH':
            return Object.assign({}, state, {auth: action.auth});
        default:
            return state
    }
};

const menuReducer = (state = {open: false, item: 'Новые заказы'}, action) => {
    switch (action.type) {
        case 'MENU_OPEN':
            return Object.assign({}, state, {open: action.open});
        case 'MENU_ACTIVE_ITEM':
            return Object.assign({}, state, {item: action.item});
        default:
            return state
    }
};

const btnReducer = (state = {btnState: 'menu'}, action) => {
    switch (action.type) {
        case 'BTN_STATE':
            return Object.assign({}, state, {btnState: action.btnState});
        default:
            return state
    }
};

const optionsReducer = (state = {optionsOpen: false, optionsSelected: 'all'}, action) => {
    switch (action.type) {
        case 'OPTIONS_OPEN':
            return Object.assign({}, state, {optionsOpen: action.optionsOpen});
        case 'OPTIONS_SELECT':
            return Object.assign({}, state, {optionsSelected: action.optionsSelected});
        default:
            return state
    }
};

const detailReducer = (state = {detail: '-100%'}, action) => {
    switch (action.type) {
        case 'DETAIL':
            return Object.assign({}, state, {detail: action.detail});
        case 'DETAIL_INFO':
            return Object.assign({}, state, {detailInfo: action.detailInfo});
        case 'DETAIL_DATA':
            return Object.assign({}, state, {detailData: action.detailData});
        default:
            return state
    }
};

const generalReducer = (state = {general: '-100%'}, action) => {
    switch (action.type) {
        case 'GENERAL':
            return Object.assign({}, state, {detail: action.detail});
        default:
            return state
    }
};

/*const formsReducer = (state = [], action) => {
    switch (action.type) {
        case 'FORMS':
            let idYes = false;
            state.forEach((el, i)=>{
                if (el.id === action.forms.id) {
                    state[i].value = action.forms.value;
                    idYes = true;
                }
            });
            if (!idYes) {
                state.push(action.forms);
            }
            return state;
        default:
            return state
    }
};*/

const formReducerInitialState = {
    formWork: {
        roof: {
            area: '0',
            height: '0',
            roofType: 'pitched',
        },
        aprons: {
            area: '0',
            apronsType: 'entry',
        },
        perimeter: {
            area: '0',
            distance: '0',
        },
    },
};

const formReducer = (state = formReducerInitialState, action) => {
    switch (action.type) {
        /*case 'FORM_WORK_KIND':
            return Object.assign({}, state, {workKind: action.workKind});
        case 'FORM_WORK':
            return Object.assign({}, state, {work: action.work});
        case 'FORM_ADDITIONALLY':
            return Object.assign({}, state, {additionally: action.additionally});
        case 'FORM_TEXT':
            return Object.assign({}, state, {formText: action.formText});
        case 'FORM_PHOTO':
            return Object.assign({}, state, {detailInfo: action.detailInfo});*/
        case 'FORM_WORK':
            return Object.assign({}, state, {formWork: action.formWork});
        default:
            return state
    }
};

const reducers = combineReducers({
    auth: authReducer,
    menu: menuReducer,
    btn: btnReducer,
    options: optionsReducer,
    detail: detailReducer,
    general: generalReducer,
    form: formReducer,
});

const store = createStore(reducers);

export default store;