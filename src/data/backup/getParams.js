let params = [
    /*{
        id: 1,
        name: 'Снег, осмотр',
        detail: {
            info: [
                {
                    id: 1,
                    type: 'text',
                    name: 'Тип работ',
                },
                {
                    id: 2,
                    type: 'text',
                    name: 'Вид работ',
                },
                {
                    id: 3,
                    type: 'text',
                    name: 'Объем работ',
                },
                {
                    id: 4,
                    type: 'text',
                    name: 'Доп. инфо',
                },
            ],
            info_position: [1,2,3,4],
            fields: [
                {
                    id: 1,
                    type: 'select',
                    name: 'Тип работ',
                    params: {
                        selectItems: ['Скатная кровля', 'Плоская кровля', 'Карниз'],
                        required: true,
                    }
                },
                {
                    id: 2,
                    type: 'number',
                    name: 'Объем работ',
                    params: {
                        required: true,
                    }
                },
                {
                    id: 3,
                    type: 'number',
                    name: 'Глубина',
                    params: {
                        required: true,
                    }
                },
                {
                    id: 4,
                    type: 'time',
                    name: 'Время осмотра',
                    params: {
                        required: true,
                    }
                },
                {
                    id: 5,
                    type: 'text',
                    name: 'Примечания',
                },
                {
                    id: 6,
                    type: 'photo',
                    name: 'Общие фото',
                    params: {
                        count: 3,
                        required: true
                    }
                },
                {
                    id: 7,
                    type: 'photo',
                    name: 'Детальные фото',
                    params: {
                        count: 5,
                        required: true
                    }
                }
            ],
            fields_position: [1,2,3,4,5,6],
        }
    },*/
    /*{
        id: 2,
        name: 'Снег, очистка',
        detail: {
            fields: [
                {
                    id: 1,
                    name: 'Время осмотра',
                    type: 'time',
                    params: {
                        required: true,
                    }
                },
                {
                    id: 2,
                    name: 'Примечания',
                    type: 'text'
                }
            ],
            fields_position: [1,2],
        }
    },*/
    {
        id: 3,
        name: 'Осмотр объекта',
        detail: {
            info: [
                {
                    id: 1,
                    type: 'text',
                    name: 'Тип работ',
                },
                {
                    id: 3,
                    type: 'text',
                    name: 'Объем работ',
                },
                {
                    id: 4,
                    type: 'text',
                    name: 'Доп. инфо',
                },
            ],
            info_position: [1,2,3,4],
            fields: [
                {
                    id: 1,
                    name: 'Время осмотра',
                    type: 'time',
                    params: {
                        required: true,
                    }
                },
                {
                    id: 2,
                    name: 'Примечания',
                    type: 'text'
                }
            ],
            fields_position: [1,2],
        }
    },
    {
        id: 4,
        name: 'Выполнение работ',
        detail: {
            info: [
                {
                    id: 1,
                    type: 'text',
                    name: 'Тип работ',
                },
                {
                    id: 3,
                    type: 'text',
                    name: 'Объем работ',
                },
                {
                    id: 4,
                    type: 'text',
                    name: 'Доп. инфо',
                },
            ],
            info_position: [1,2,3,4],
            fields: [
                {
                    id: 1,
                    name: 'Время осмотра',
                    type: 'time',
                    params: {
                        required: true,
                    }
                },
                {
                    id: 2,
                    name: 'Примечания',
                    type: 'text'
                }
            ],
            fields_position: [1,2],
        }
    }
];

export default params;