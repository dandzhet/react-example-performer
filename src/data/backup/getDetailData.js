let lead1;
let lead2;

let leads = [];

lead1 = {
    id: 1,
    type_id: 1,
    active: true,
    detail: {
        title: 'Герцена 63',
        status: 'Активно',
        contacts: [
            {
                name: 'Антон Герцин',
                phone: '+79131234567',
            }
        ],
        info: [
            {
                id: 1,
                text: 'Снег',
            },
            {
                id: 2,
                text: 'Скатная кровля',
            },
            {
                id: 3,
                text: '120',
            },
            {
                id: 4,
                text: '---',
            }
        ],
    }
};

lead2 = {
    id: 2,
    type_id: 1,
    active: true,
    detail: {
        title: 'Красноармейская 141',
        status: 'Активно',
        contacts: [
            {
                name: 'Дмитрий Красноармейский',
                phone: '+79133214567',
            },
            {
                name: 'Марина Дмитриевна',
                phone: '+79133211234',
            }
        ],
        info: [
            {
                id: 1,
                text: 'Снег',
            },
            {
                id: 2,
                text: 'Плоская кровля',
            },
            {
                id: 3,
                text: '200',
            },
            {
                id: 4,
                text: 'Много наледи',
            }
        ],
    }
};

leads.push(lead1);
leads.push(lead2);

let getLeads = (id, callback)=>{
    let lead;
    leads.forEach((el)=>{
        if (el.id === id) {
            lead = el;
        }
    });
    return callback(lead);
}

export default getLeads;