let leads = [
    {
        id: 1,
        type: 1,
        active: true,
        info: {
            title: 'Герцена 63',
            text: 'Снег, Скатная кровля',
            status: 'Активно'
        }
    },
    {
        id: 2,
        type: 1,
        active: true,
        info: {
            title: 'Красноармейская 141',
            text: 'Снег, Плоская кровля',
            status: 'Активно'
        }
    }
];

export default leads;