let params = [
    {
        id: 3,
        name: 'Осмотр объекта', // name in performer menu
        detail: {
            info: [
                {
                    id: 1,
                    type: 'text',
                    name: 'Тип работ',
                },
                {
                    id: 3,
                    type: 'text',
                    name: 'Объем работ',
                },
                {
                    id: 4,
                    type: 'text',
                    name: 'Доп. инфо',
                },
            ],
            info_position: [],// position of info fields
            fields: [
                {
                    id: 1,
                    name: 'Время осмотра',
                    type: 'time',
                    params: {
                        required: true,
                    }
                },
                {
                    id: 2,
                    name: 'Примечания',
                    type: 'text'
                }
            ],
            fields_position: [],// position of form fields
        }
    },
    {
        id: 4,
        name: 'Выполнение работ',
        detail: {
            info: [
                {
                    id: 1,
                    type: 'text',
                    name: 'Тип работ',
                },
                {
                    id: 3,
                    type: 'text',
                    name: 'Объем работ',
                },
                {
                    id: 4,
                    type: 'text',
                    name: 'Доп. инфо',
                },
            ],
            info_position: [],
            fields: [
                {
                    id: 1,
                    name: 'Время осмотра',
                    type: 'time',
                    params: {
                        required: true,
                    }
                },
                {
                    id: 2,
                    name: 'Примечания',
                    type: 'text'
                }
            ],
            fields_position: [],
        }
    }
];

export default params;